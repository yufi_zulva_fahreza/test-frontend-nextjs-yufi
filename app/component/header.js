import styles from '../styles/Style.module.css';

export default function Navbar() {
  return (
    <>
      <div className="container mx-auto px-24">
        <div className="max-w-screen-xl flex item-center mt-12">
          <h1 className={styles.logo}>Food Ajah</h1>
          <ul className={styles.logo} style={{ fontSize: '14px' }}>
            <li className="inline-block mx-9 transition duration-300 border-b-2 border-transparent hover:border-orange-500 hover:text-orange-500"><a href=''>Sarapan</a></li>
            <li className="inline-block mx-9 transition duration-300 border-b-2 border-transparent hover:border-orange-500 hover:text-orange-500"><a href=''>Makan Siang</a></li>
            <li className="inline-block mx-9 transition duration-300 border-b-2 border-transparent hover:border-orange-500 hover:text-orange-500"><a href=''>Makan Malam</a></li>
            <li className={styles.icon}>
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="22" viewBox="0 0 20 22" fill="none">
                <g clip-path="url(#clip0_14901_136)">
                  <path d="M4 7V5C4 3.67392 4.52678 2.40215 5.46447 1.46447C6.40215 0.526784 7.67392 0 9 0C10.3261 0 11.5979 0.526784 12.5355 1.46447C13.4732 2.40215 14 3.67392 14 5V7H17C17.2652 7 17.5196 7.10536 17.7071 7.29289C17.8946 7.48043 18 7.73478 18 8V20C18 20.2652 17.8946 20.5196 17.7071 20.7071C17.5196 20.8946 17.2652 21 17 21H1C0.734784 21 0.48043 20.8946 0.292893 20.7071C0.105357 20.5196 0 20.2652 0 20V8C0 7.73478 0.105357 7.48043 0.292893 7.29289C0.48043 7.10536 0.734784 7 1 7H4ZM4 9H2V19H16V9H14V11H12V9H6V11H4V9ZM6 7H12V5C12 4.20435 11.6839 3.44129 11.1213 2.87868C10.5587 2.31607 9.79565 2 9 2C8.20435 2 7.44129 2.31607 6.87868 2.87868C6.31607 3.44129 6 4.20435 6 5V7Z" fill="#090909"/>
                  <circle cx="17" cy="19" r="3" fill="#FF922C"/>
                </g>
              </svg>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}