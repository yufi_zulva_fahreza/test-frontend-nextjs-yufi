"use client"
import Image from "next/image"
import styles from "../styles/Style.module.css"
import React, { useState, useEffect } from "react";

const MenuImage = () => {

    return (
        <>
            <Image 
                className={styles.image1}
                src="/img1.svg"
                alt=""
                width={100}
                height={100}
            />

            <Image
                className={styles.image2}
                src="/img2.svg"
                alt=""
                width={100}
                height={100}
            />

            <Image 
                className={styles.image3}
                src="/img3.svg"
                alt=""
                width={100}
                height={100}
            />

            <Image
                className={styles.image4}
                src="/img4.svg"
                alt=""
                width={100}
                height={100}
            />

            <Image
                className={styles.image5}
                src="/img5.svg"
                alt=""
                width={100}
                height={100}
            />

            <Image
                className={styles.image6}
                src="/img1.svg"
                alt=""
                width={280}
                height={280}
            />

        </>
    )
}

export default MenuImage