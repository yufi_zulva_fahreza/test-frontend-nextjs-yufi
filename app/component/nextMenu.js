"use client"
import Image from "next/image"
import styles from "../styles/Style.module.css"
import React from "react"

const NextMenu = ({menuItems, btnNext, setBtnNext}) => {

  const currentIndex = menuItems.findIndex((item) => item === btnNext);

  const handleBtnNext = () => {
    const nextIndex = (currentIndex + 1) % menuItems.length;
    setBtnNext(menuItems[nextIndex]);
  };

  const handleBtnPrevious = () => {
    const previousIndex =
      (currentIndex - 1 + menuItems.length) % menuItems.length;
    setBtnNext(menuItems[previousIndex]);
  };

    return (
        <div className={styles.iconContainer}>
            <ul className={styles.iconList}>
                <li>
                    <Image
                        className={styles.icon1}
                        src="/icon1.svg"
                        alt=""
                        width={80}
                        height={80}
                        onClick={handleBtnNext}
                    />
                </li>
                <li>
                    <Image
                        className={styles.icon2}
                        src="/icon2.svg"
                        alt=""
                        width={80}
                        height={80}
                        onClick={handleBtnPrevious}
                    />
                </li>
            </ul>
        </div>
    )
}

export default NextMenu;