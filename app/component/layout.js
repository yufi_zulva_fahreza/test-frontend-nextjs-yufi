"use client"
import React, { useState, useEffect } from 'react';
import styles from '../styles/Style.module.css';
import Button from './button';
import NextMenu from './nextMenu';

const Content = () => {

  const menuItems = [
    {
        price: "Rp 45.000",
        name: "Salad Ayam\nKacang Tahu",
        desc: "Lorem ipsum dolor sit amet consectetur adipisicing\nelit. Quod consectetur odio ducimus laboriosam\nvero dolores?"
    },
    {
        price: "Rp 23.000",
        name: "Salad Timun Dan\nUdang Segar",
        desc: "Lorem ipsum dolor sit amet consectetur adipisicing\nelit. Quod consectetur odio ducimus laboriosam\nvero dolores?"
    },
    {
        price: "Rp 44.000",
        name: "Dada Ayam Daun\nSelada",
        desc: "Lorem ipsum dolor sit amet consectetur adipisicing\nelit. Quod consectetur odio ducimus laboriosam\nvero dolores?"
    }
  ];

  const [btnNext, setBtnNext] = useState(menuItems[0]);
  const [currentIndex, setCurrentIndex] = useState(0);

  const formatNewRow = (newrom) => {
    return newrom.split('\n').map((line, index) => (
      <React.Fragment key={index}>
        {line}
        {index < newrom.split('\n').length - 1 && <br />}
      </React.Fragment>
    ));
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIndex((prevIndex) => (prevIndex + 1) % menuItems.length);
    }, 4000);

    return () => clearInterval(interval);
  }, [menuItems.length]);

  useEffect(() => {
    setBtnNext(menuItems[currentIndex]);
  }, [currentIndex, menuItems]);

  return (
    <>
      <div className="container px-8 mx-auto">
        <div className={styles.content}>
          <p className={styles.price}>{btnNext.price}</p>
          <div className={styles.textContainer}>
            <p className={styles.nameMenu}>{formatNewRow(btnNext.name)}</p>
            <div className={styles.desc}>{formatNewRow(btnNext.desc)}</div>
          </div>
        </div>
      </div>
      <Button />

      <div>
        {/* icon next & previous menu */}
        <NextMenu btnNext={btnNext} setBtnNext={setBtnNext} menuItems={menuItems} />
      </div>
    </>
  );
};

export default Content;
