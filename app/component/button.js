import '../styles/Style.module.css'

const Button = () => {
    return(
        <>
            <button type="submit" className="bg-orange-400 hover:bg-orange-500 text-white font-bold py-3 px-10 rounded-full" style={{ fontSize: '13px', marginLeft: '6%', marginTop: '2.8%', boxShadow: '0px 16px 18px rgba(255, 140, 0, 0.5)' }}>
                Beli Sekarang
            </button>

        </>
    )
}

export default Button