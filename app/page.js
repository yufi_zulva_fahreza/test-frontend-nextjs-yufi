import Ellipse from './component/ellipse'
import Ellipse2 from './component/ellipse2'
import Navbar from './component/header'
import Content from './component/layout'
import MenuImage from './component/menuImage'
import styles from './styles/Style.module.css'

const Home = () => {

  return (
    <>
       <div style={{ position: 'relative', overflow: 'hidden' }}>
        <Ellipse />
        <Navbar />
        <Ellipse2 />
        <div className="container mx-auto px-10" style={{ maxWidth: '100%' }}>
          <Content />

          {/* Menu Image */}
          <MenuImage />

          <p className={styles.link}><a href=''>Butuh Menu Lain?</a></p>
        </div>
      </div>
    </>
  );
}

export default Home